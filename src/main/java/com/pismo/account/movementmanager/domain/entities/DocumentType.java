package com.pismo.account.movementmanager.domain.entities;

public enum DocumentType {

    CPF,
    CNPJ;

}
