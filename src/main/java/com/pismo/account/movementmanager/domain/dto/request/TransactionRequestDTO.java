package com.pismo.account.movementmanager.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionRequestDTO {

    @NotNull
    @JsonProperty("operation_type_id")
    private Long operationType;

    @NotNull
    @JsonProperty("account_id")
    private Long account;

    @NotNull
    private BigDecimal amount;

}
