package com.pismo.account.movementmanager.domain.dto.response;

import com.pismo.account.movementmanager.domain.entities.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountResponseDTO {

    private Long accountId;
    private String documentNumber;
    private DocumentType documentType;
    private BigDecimal balance;

}
