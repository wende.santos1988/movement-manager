package com.pismo.account.movementmanager.domain.dto.response;

import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.entities.OperationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionResponseDTO {
    
    private Long transactionId;
    private OperationType operationType;
    private Account account;
    private BigDecimal amount;
    private LocalDateTime eventDate;
}
