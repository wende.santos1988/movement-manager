package com.pismo.account.movementmanager.domain.repositories;

import com.pismo.account.movementmanager.domain.entities.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationTypeRepository extends JpaRepository<OperationType, Long> {
}
