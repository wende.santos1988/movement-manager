package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Transaction;

import java.util.List;

public interface ITransactionService {

    List<Transaction> findAll();

    Transaction save(TransactionRequestDTO transaction);

}
