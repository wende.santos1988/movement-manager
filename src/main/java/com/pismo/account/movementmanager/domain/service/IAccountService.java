package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.entities.Account;

import java.util.Optional;

public interface IAccountService {

    public Optional<Account> findById(Long id);

    public Account save(Account account);

}
