package com.pismo.account.movementmanager.domain.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
@NoArgsConstructor
@Table(name = Account.ENTITY_NAME)
public class Account {

    public static final String ENTITY_NAME = "ACCOUNT";

    public Account(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @Id
    @Column(name = "ACCOUNT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long accountId;

    @Column(name = "DOCUMENT_NUMBER")
    @NotNull
    private String documentNumber;

    @Column(name = "BALANCE")
    @NotNull
    private BigDecimal balance;

}
