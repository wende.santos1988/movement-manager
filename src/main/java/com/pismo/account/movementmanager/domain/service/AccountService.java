package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.ValidationNames;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.repositories.AccountRepository;
import com.pismo.account.movementmanager.exceptions.BusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService implements IAccountService {

    private final AccountRepository accountRepository;

    private AccountRepository getRepository() {
        return accountRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Account> findById(Long id) {
        return getRepository().findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Account save(Account account) {
        log.debug("method=save, message=process to create a new account, document: {}", account.getDocumentNumber());

        if (accountRepository.existsByDocumentNumber(account.getDocumentNumber())) {
            throw new BusinessException(ValidationNames.ACCOUNT_DOCUMENT_SAVED);
        }
        return getRepository().save(account);
    }

}
