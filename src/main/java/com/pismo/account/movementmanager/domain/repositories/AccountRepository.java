package com.pismo.account.movementmanager.domain.repositories;

import com.pismo.account.movementmanager.domain.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    boolean existsByDocumentNumber(String documentNumber);
}
