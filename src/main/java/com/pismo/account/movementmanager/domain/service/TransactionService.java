package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.entities.OperationType;
import com.pismo.account.movementmanager.domain.entities.Transaction;
import com.pismo.account.movementmanager.domain.repositories.AccountRepository;
import com.pismo.account.movementmanager.domain.repositories.OperationTypeRepository;
import com.pismo.account.movementmanager.domain.repositories.TransactionRepository;
import com.pismo.account.movementmanager.exceptions.BusinessException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.pismo.account.movementmanager.domain.ValidationNames.TRANSACTION_ACCOUNT_NOT_FOUND;
import static com.pismo.account.movementmanager.domain.ValidationNames.TRANSACTION_OPERATION_TYPE_NOT_FOUND;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionService implements ITransactionService {

    private final TransactionRepository transactionRepository;

    private final AccountRepository accountRepository;

    private final OperationTypeRepository operationTypeRepository;

    private TransactionRepository getRepository() {
        return transactionRepository;
    }

    @Override
    public List<Transaction> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Transaction save(TransactionRequestDTO transactionRequestDTO) {
        Transaction transaction = createTransaction(transactionRequestDTO);

        Account accountSaved = accountRepository.findById(transaction.getAccount().getAccountId())
                .orElseThrow(() -> new BusinessException("Account not found to id: " + transaction.getAccount().getAccountId()));

        validAccountBalanceWithTransactionAmount(transaction, accountSaved);

        accountSaved.setBalance(accountSaved.getBalance().subtract(transaction.getAmount()));
        accountRepository.save(accountSaved);

        return getRepository().save(transaction);
    }

    private static void validAccountBalanceWithTransactionAmount(Transaction transaction, Account accountSaved) {
        if (transaction.getAmount().compareTo(accountSaved.getBalance()) > 0){
            throw new BusinessException("Amount invalid for operation");
        }
    }

    public Transaction createTransaction(TransactionRequestDTO transactionRequestDTO) {
        log.debug("method=save, message=process to register new transaction, accountID: {}.",
                transactionRequestDTO.getAccount());

        Transaction transaction = new Transaction();
        transaction.setAmount(transactionRequestDTO.getAmount());
        transaction.setOperationType(findOperationTypeById(transactionRequestDTO.getOperationType()));
        transaction.setAccount(findAccountById(transactionRequestDTO.getAccount()));
        transaction.setEventDate(LocalDateTime.now());
        return transaction;
    }

    private OperationType findOperationTypeById(Long id) {
        return operationTypeRepository.findById(id)
                .orElseThrow(() -> new BusinessException(TRANSACTION_OPERATION_TYPE_NOT_FOUND));
    }

    private Account findAccountById(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new BusinessException(TRANSACTION_ACCOUNT_NOT_FOUND));
    }

}
