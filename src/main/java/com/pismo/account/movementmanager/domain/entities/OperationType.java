package com.pismo.account.movementmanager.domain.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = OperationType.ENTITY_NAME)
public class OperationType {

    public static final String ENTITY_NAME = "OPERATION_TYPES";

    @Id
    @Column(name = "OPERATION_TYPE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long operationTypeId;

    @Column(name = "DESCRIPTION")
    private String description;

}
