package com.pismo.account.movementmanager.domain.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = Transaction.ENTITY_NAME)
public class Transaction {

    public static final String ENTITY_NAME = "TRANSACTION";

    @Id
    @Column(name = "TRANSACTION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "OPERATION_TYPE_ID", referencedColumnName = "OPERATION_TYPE_ID", nullable = false)
    private OperationType operationType;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ACCOUNT_ID", nullable = false)
    private Account account;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "EVENT_DATE")
    private LocalDateTime eventDate;

    @PrePersist
    public void prePersist() {
        this.eventDate = LocalDateTime.now();
    }


}
