package com.pismo.account.movementmanager.domain;

public class ValidationNames {
    public static final String ACCOUNT_DOCUMENT_SAVED = "the number of document saved in database";
    public static final String TRANSACTION_ACCOUNT_NOT_FOUND = "the id of account not found in database";
    public static final String TRANSACTION_OPERATION_TYPE_NOT_FOUND = "the id of operation type not found in database";
    public static final String TRANSACTION_AMOUNT_REQUIRED = "the value is required";
    public static final String TRANSACTION_EVENT_DATE_REQUIRED = "the event date is required";
}
