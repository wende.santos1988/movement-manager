package com.pismo.account.movementmanager.api.v1.controllers;

import com.pismo.account.movementmanager.domain.dto.request.AccountRequestDTO;
import com.pismo.account.movementmanager.domain.dto.response.AccountResponseDTO;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.service.IAccountService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@Slf4j
@RestController
@RequestMapping("/accounts")
public class AccountController {

    private static final String ENDPOINT_VERSION_1 = "/v1";

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(ENDPOINT_VERSION_1 + "/{id}")
    public AccountResponseDTO findById(@PathVariable Long id) {
        log.debug("method=findById, message=find account by id: {}", id);

        return accountService
            .findById(id)
            .map(this::convertToDTO)
            .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    @PostMapping(ENDPOINT_VERSION_1)
    @ResponseStatus(CREATED)
    public AccountResponseDTO createAccount(@Valid @RequestBody AccountRequestDTO accountRequestDTO) {
        log.debug("method=createAccount, message=create new account to document: {}",
                accountRequestDTO.getDocumentNumber());

        Account account = convertToEntity(accountRequestDTO);
        account = accountService.save(account);

        log.debug("method=createAccount, message=account created.");
        return convertToDTO(account);
    }

    private AccountResponseDTO convertToDTO(Account account) {
        return modelMapper.map(account, AccountResponseDTO.class);
    }

    private Account convertToEntity(AccountRequestDTO accountRequestDTO) {
        return modelMapper.map(accountRequestDTO, Account.class);
    }

}
