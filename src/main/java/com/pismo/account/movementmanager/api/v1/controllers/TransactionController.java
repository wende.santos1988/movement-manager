package com.pismo.account.movementmanager.api.v1.controllers;

import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.dto.response.TransactionResponseDTO;
import com.pismo.account.movementmanager.domain.entities.Transaction;
import com.pismo.account.movementmanager.domain.service.ITransactionService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@Slf4j
@RestController
@RequestMapping("/v1/transactions")
public class TransactionController {

    private static final String ENDPOINT_VERSION_1 = "/v1";

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping(ENDPOINT_VERSION_1)
    @ResponseStatus(CREATED)
    public TransactionResponseDTO createTransaction(@Valid @RequestBody TransactionRequestDTO transactionRequestDTO) {
        log.debug("method=createAccount, message=create new transaction to account: {}",
                transactionRequestDTO.getAccount());

        Transaction transaction = transactionService.save(transactionRequestDTO);

        log.debug("method=createTransaction, message=transaction registered.");
        return convertToDTO(transaction);
    }

    private TransactionResponseDTO convertToDTO(Transaction transaction) {
        return modelMapper.map(transaction, TransactionResponseDTO.class);
    }

    private Transaction convertToEntity(TransactionRequestDTO transactionRequestDTO) {
        return modelMapper.map(transactionRequestDTO, Transaction.class);
    }


}
