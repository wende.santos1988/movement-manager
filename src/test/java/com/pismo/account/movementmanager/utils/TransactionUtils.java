package com.pismo.account.movementmanager.utils;

import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.entities.OperationType;
import com.pismo.account.movementmanager.domain.entities.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.pismo.account.movementmanager.utils.AccountUtils.createAccountWithId;

public class TransactionUtils {

    public static final int INTERACTIONS = 10;

    public static TransactionRequestDTO createTransactionDTOValid() {
        TransactionRequestDTO transactionRequestDTO = new TransactionRequestDTO();
        transactionRequestDTO.setAccount(1L);
        transactionRequestDTO.setOperationType(1L);
        transactionRequestDTO.setAmount(new BigDecimal(1000));
        return transactionRequestDTO;
    }

    public static Transaction createTransactionValid() {
        Transaction transaction = new Transaction();
        transaction.setAccount(createAccountWithId());
        transaction.setOperationType(getOperationType());
        transaction.setEventDate(LocalDateTime.now());
        transaction.setAmount(new BigDecimal(1000));
        return transaction;
    }

    public static List<Transaction> createListTransactionOfTenItems() {
        List<Transaction> transactionList = new ArrayList<>();

        for (int i = 0; i < INTERACTIONS; i++) {
            Transaction transaction = createTransactionValid();
            transaction.setAmount(transaction.getAmount().multiply(new BigDecimal(INTERACTIONS)));
            transactionList.add(transaction);
        }
        return transactionList;
    }

    public static Transaction createTransactionWithId() {
        Transaction transactionValid = createTransactionValid();
        transactionValid.setTransactionId(1L);
        return transactionValid;
    }

    private static OperationType getOperationType() {
        OperationType operationType = new OperationType();
        operationType.setOperationTypeId(1L);
        operationType.setDescription("COMPRA A VISTA");
        return operationType;
    }

}
