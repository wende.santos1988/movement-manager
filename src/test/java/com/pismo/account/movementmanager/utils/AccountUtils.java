package com.pismo.account.movementmanager.utils;

import com.pismo.account.movementmanager.domain.dto.request.AccountRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.entities.DocumentType;

public class AccountUtils {

    public static final String DOCUMENT_NUMBER = "00299620212";

    public static Account createValidAccount() {
        return new Account(DOCUMENT_NUMBER);
    }

    public static AccountRequestDTO createValidAccountDto() {
        return new AccountRequestDTO(DOCUMENT_NUMBER);
    }

    public static Account createAccountWithId() {
        Account account = createValidAccount();
        account.setAccountId(1L);
        return account;
    }

}
