package com.pismo.account.movementmanager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Transaction;
import com.pismo.account.movementmanager.domain.service.ITransactionService;
import com.pismo.account.movementmanager.api.v1.controllers.TransactionController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.pismo.account.movementmanager.utils.TransactionUtils.createTransactionDTOValid;
import static com.pismo.account.movementmanager.utils.TransactionUtils.createTransactionWithId;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TransactionController.class)
@AutoConfigureWebMvc
class TransactionControllerTest {

    static String URL_API = "/transaction";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ITransactionService transactionService;

    @Test
    @DisplayName("should create transaction with success")
    public void shouldCreateTransactionWithSuccess() throws Exception {

        TransactionRequestDTO transactionRequestDTO = createTransactionDTOValid();
        Transaction transaction = createTransactionWithId();

        BDDMockito.given(transactionService.save(Mockito.any(TransactionRequestDTO.class))).willReturn(transaction);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        String json = objectMapper.writeValueAsString(transactionRequestDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(URL_API)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
            .andExpect(status().isCreated())
            .andExpect(jsonPath("transactionId").isNotEmpty());
    }
}