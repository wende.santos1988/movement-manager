package com.pismo.account.movementmanager.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pismo.account.movementmanager.domain.dto.request.AccountRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.service.IAccountService;
import com.pismo.account.movementmanager.api.v1.controllers.AccountController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static com.pismo.account.movementmanager.utils.AccountUtils.createAccountWithId;
import static com.pismo.account.movementmanager.utils.AccountUtils.createValidAccountDto;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AccountController.class)
@AutoConfigureWebMvc
class AccountControllerTest {

    static String ACCOUNT_URL_API = "/account";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    IAccountService accountService;

    @Test
    @DisplayName("should create Account with success")
    public void shouldCreateAccountWithSuccess() throws Exception {
        AccountRequestDTO requestDTO = createValidAccountDto();
        Account account = createAccountWithId();

        BDDMockito.given(accountService.save(Mockito.any(Account.class))).willReturn(account);
        String json = new ObjectMapper().writeValueAsString(requestDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(ACCOUNT_URL_API)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("accountId").isNotEmpty())
                .andExpect(jsonPath("documentNumber").value(requestDTO.getDocumentNumber()));
    }

    @Test
    @DisplayName("should throw an error when the fields provided are not enough to create an account")
    public void shouldThrowAnErrorWhenTheFieldsProvidedAreNotEnoughToCreateAnAccount() throws Exception {
        String json = new ObjectMapper().writeValueAsString(new AccountRequestDTO());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(ACCOUNT_URL_API)
                .contentType(APPLICATION_JSON)
                .accept(APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.hasSize(1)));
    }

    @Test
    @DisplayName("should find information of test")
    public void shouldFindInformationOfAccount() throws Exception {
        Long id = 1L;
        Account account = createAccountWithId();
        account.setAccountId(id);

        BDDMockito.given(accountService.findById(id)).willReturn(Optional.of(account));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
            .get(ACCOUNT_URL_API.concat("/" + id))
            .accept(APPLICATION_JSON);

        mockMvc
            .perform(request)
            .andExpect(status().isOk())
            .andExpect(jsonPath("accountId").value(id));
    }

    @Test
    @DisplayName("should return not found when account  not exists")
    public void shouldReturnNotFoundWhenAccountNotExists() throws Exception{
        long id = 1L;
        BDDMockito.given(accountService.findById(Mockito.anyLong())).willReturn(Optional.empty());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
            .get(ACCOUNT_URL_API.concat("/" + id))
            .accept(APPLICATION_JSON);

        mockMvc.perform(request).andExpect(status().isNotFound());
    }
}