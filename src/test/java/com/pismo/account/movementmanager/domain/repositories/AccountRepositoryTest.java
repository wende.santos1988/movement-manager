package com.pismo.account.movementmanager.domain.repositories;

import com.pismo.account.movementmanager.domain.entities.Account;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.pismo.account.movementmanager.utils.AccountUtils.createValidAccount;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class AccountRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    AccountRepository accountRepository;

    @Test
    @DisplayName("should save a account")
    public void shouldSaveAAccount() {
        Account account = createValidAccount();

        Account accountSaved = accountRepository.save(account);
        assertEquals(accountSaved.getDocumentNumber(), account.getDocumentNumber());
    }

    @Test
    @DisplayName("should delete a account")
    public void shouldNotSaveAAccountWhenDocumentIsNull() {
        Account account = createValidAccount();

        entityManager.persist(account);
        Account accountSaved = entityManager.find(Account.class, 1L);

        accountRepository.delete(accountSaved);
        Account accountDeleted = entityManager.find(Account.class, 1L);

        assertNull(accountDeleted);
    }

}