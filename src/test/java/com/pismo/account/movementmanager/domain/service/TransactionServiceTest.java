package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.ValidationNames;
import com.pismo.account.movementmanager.domain.dto.request.TransactionRequestDTO;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.entities.OperationType;
import com.pismo.account.movementmanager.domain.entities.Transaction;
import com.pismo.account.movementmanager.domain.repositories.AccountRepository;
import com.pismo.account.movementmanager.domain.repositories.OperationTypeRepository;
import com.pismo.account.movementmanager.domain.repositories.TransactionRepository;
import com.pismo.account.movementmanager.exceptions.BusinessException;
import com.pismo.account.movementmanager.utils.AccountUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static com.pismo.account.movementmanager.utils.TransactionUtils.createTransactionDTOValid;
import static com.pismo.account.movementmanager.utils.TransactionUtils.createTransactionWithId;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.when;

class TransactionServiceTest {

    ITransactionService service;

    TransactionRepository transactionRepository;

    OperationTypeRepository operationTypeRepository;

    AccountRepository accountRepository;

    @BeforeEach
    public void setUp() {
        transactionRepository = mock(TransactionRepository.class);
        operationTypeRepository = mock(OperationTypeRepository.class);
        accountRepository = mock(AccountRepository.class);
        service = new TransactionService(transactionRepository, accountRepository, operationTypeRepository);
    }

    @Test
    @DisplayName("should save a transaction")
    public void shouldSaveATransaction() {
        TransactionRequestDTO transactionRequestDTO = createTransactionDTOValid();
        Transaction transactionWithId = createTransactionWithId();

        OperationType operationType = new OperationType();
        operationType.setOperationTypeId(1L);
        Account accountWithId = AccountUtils.createAccountWithId();

        given(accountRepository.findById(anyLong())).willReturn(Optional.of(accountWithId));
        given(operationTypeRepository.findById(anyLong())).willReturn(Optional.of(operationType));
        given(transactionRepository.save(Mockito.any(Transaction.class))).willReturn(transactionWithId);

        when(service.save(transactionRequestDTO)).thenReturn(transactionWithId);

        Transaction transactionSaved = service.save(transactionRequestDTO);

        assertNotNull(transactionSaved);
        assertEquals(
                transactionSaved.getAccount().getDocumentNumber(), transactionSaved.getAccount().getDocumentNumber());
        assertEquals(transactionSaved.getTransactionId(), 1L);
    }

    // TODO: tests the exceptions
    @Test
    @DisplayName("should a throw business exception when account not save")
    public void shouldAThrowBusinessExceptionWhenAccountNotSave() {
        TransactionRequestDTO transactionRequestDTO = createTransactionDTOValid();
        transactionRequestDTO.setAccount(1000L);
        Transaction transactionWithId = createTransactionWithId();
        OperationType operationType = new OperationType();
        operationType.setOperationTypeId(1L);

        given(operationTypeRepository.findById(anyLong())).willReturn(Optional.of(operationType));
        given(accountRepository.findById(anyLong())).willReturn(Optional.empty());

        Throwable exception = Assertions.catchThrowable(() -> service.save(transactionRequestDTO));

        Assertions.assertThat(exception)
            .isInstanceOf(BusinessException.class)
            .hasMessage(ValidationNames.TRANSACTION_ACCOUNT_NOT_FOUND);

        Mockito.verify(transactionRepository, Mockito.never()).save(transactionWithId);
    }

    @Test
    @DisplayName("should a throw business exception when operation type not save")
    public void shouldAThrowBusinessExceptionWhenOperationTypeNotSave() {
        TransactionRequestDTO transactionRequestDTO = createTransactionDTOValid();
        transactionRequestDTO.setAccount(1L);
        Transaction transactionWithId = createTransactionWithId();
        Account accountValid = AccountUtils.createAccountWithId();

        given(operationTypeRepository.findById(anyLong())).willReturn(Optional.empty());
        given(accountRepository.findById(anyLong())).willReturn(Optional.of(accountValid));

        Throwable exception = Assertions.catchThrowable(() -> service.save(transactionRequestDTO));

        Assertions.assertThat(exception)
                .isInstanceOf(BusinessException.class)
                .hasMessage(ValidationNames.TRANSACTION_OPERATION_TYPE_NOT_FOUND);

        Mockito.verify(transactionRepository, Mockito.never()).save(transactionWithId);
    }

}