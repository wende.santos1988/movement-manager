package com.pismo.account.movementmanager.domain.repositories;

import com.pismo.account.movementmanager.domain.entities.Transaction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.pismo.account.movementmanager.utils.TransactionUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class TransactionRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    TransactionRepository transactionRepository;

    @Test
    @DisplayName("should save a a transaction")
    public void shouldSaveAAccount() {
        Transaction transaction = createTransactionValid();

        Transaction accountSaved = transactionRepository.save(transaction);
        assertEquals(accountSaved.getAccount(), transaction.getAccount());
    }

    @Test
    @DisplayName("should find list transactions of Account")
    public void shouldFindListTransactionOfAccount() {
        List<Transaction> createListTransaction = createListTransactionOfTenItems();

        createListTransaction.forEach(t -> {
            entityManager.persist(t);
        });

        List<Transaction> transactionList = transactionRepository.findAll();

        assertNotNull(transactionList);
        assertEquals(INTERACTIONS, transactionList.size());
    }

}