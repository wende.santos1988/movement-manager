package com.pismo.account.movementmanager.domain.service;

import com.pismo.account.movementmanager.domain.ValidationNames;
import com.pismo.account.movementmanager.domain.entities.Account;
import com.pismo.account.movementmanager.domain.repositories.AccountRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static com.pismo.account.movementmanager.utils.AccountUtils.createAccountWithId;
import static com.pismo.account.movementmanager.utils.AccountUtils.createValidAccount;
import static org.junit.jupiter.api.Assertions.*;

public class AccountServiceTest {

    IAccountService service;

    AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        accountRepository = Mockito.mock(AccountRepository.class);
        this.service = new AccountService(accountRepository);
    }

    @Test
    @DisplayName("should save a account")
    public void shouldSaveAAccount() {
        Account account = createValidAccount();
        Account accountWithID = createAccountWithId();

        Mockito.when(accountRepository.existsByDocumentNumber(account.getDocumentNumber())).thenReturn(false);
        Mockito.when(accountRepository.save(account)).thenReturn(accountWithID);

        Account accountSaved = service.save(account);

        assertNotNull(accountSaved);
        assertEquals(accountSaved.getDocumentNumber(), account.getDocumentNumber());
        assertEquals(accountSaved.getAccountId(), 1L);
    }

    @Test
    @DisplayName("should NOT save a account WHEN document number is saved")
    public void shouldNOTSaveAAccountWHENDocumentNumberIsNull() {
        Account account = createValidAccount();
        account.setDocumentNumber(null);

        Mockito.when(accountRepository.existsByDocumentNumber(account.getDocumentNumber())).thenReturn(true);
        Throwable exception =  Assertions.catchThrowable(() -> service.save(account));

        Assertions.assertThat(exception)
            .isInstanceOf(Exception.class)
            .hasMessage(ValidationNames.ACCOUNT_DOCUMENT_SAVED);

        Mockito.verify(accountRepository, Mockito.never()).save(account);
    }

    @Test
    @DisplayName("Should return account saved")
    public void shouldReturnAccountSaved() {
        Long id = 1L;
        Account saved = createAccountWithId();

        Mockito.when(accountRepository.findById(id)).thenReturn(Optional.of(saved));

        Optional<Account> accountSaved = service.findById(id);

        assertTrue(accountSaved.isPresent());
        assertEquals(accountSaved.get().getAccountId(), id);
    }

    @Test
    @DisplayName("should return empty when trying to get a book that doesn't exist in the database")
    public void shouldReturnEmptyWhenTryingToGetABookThatDoesntExistInTheDatabase() {
        Long id = 1L;

        Mockito.when(accountRepository.findById(id)).thenReturn(Optional.empty());

        Optional<Account> accountSaved = service.findById(id);

        assertFalse(accountSaved.isPresent());
    }
}