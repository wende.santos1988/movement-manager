# Movement Manager API
#### Project to management movements.

API desenvolvida para teste técnico.

- SpringBoot, JPA, H2DataBase

### Funcionalidades

- Castratro e listagem de Account
- Cadastro de Transaction


### 🔧 Utilização
- Fazer o clone do projeto;
- Rodar o comando a seguir com docker para gerar imagem:
```
docker build -t movement-manager:1.0 .
```
- apos gerar imagem rodar o comando para criar o container e subir a aplicação:
```
docker run -d --name movement-manager -p 8080:8080 movement-manager:1.0
```
- apos isso a aplicação estara disponivel na porta *:8080* com o path *movement-manager*
- documentação da API:
```
http://localhost:8080/movement-manager/swagger-ui/index.html#/
```
- acesso a base de dados H2, a *JDBC URL: jdbc:h2:mem:testdb*, *user: sa*, senha não precisa
```
http://localhost:8080/movement-manager/h2-console/login.do?
```
#### Os endpoints então distribuidos da seguinte maneira
##### Account
- para buscar uma *Account*
```
GET
/account/ID
```
- para criar uma nova Account
```
POST
/account/
```
com o modelo de payload abaixo
````
{
  "documentNumber": "string"
}
````
##### Transaction
- para criar uma nova Transaction
```
POST
/transaction/
```
com o modelo de payload abaixo
````
{
  "account_id": 1,
  "amount": 0,
  "operation_type_id": 1
}
````