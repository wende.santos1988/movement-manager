FROM gradle:jdk11 as gradleimage

COPY . /home/gradle/source

WORKDIR /home/gradle/source

RUN gradle build

FROM openjdk

RUN mkdir -p app/movement-manager

#COPY build/libs/movementmanager-0.0.1-SNAPSHOT.jar app/movement-manager
COPY --from=gradleimage /home/gradle/source/build/libs/movementmanager-0.0.1-SNAPSHOT.jar app/movement-manager

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/movement-manager/movementmanager-0.0.1-SNAPSHOT.jar"]